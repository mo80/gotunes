﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoTunes.Controllers
{
    /// <summary>
    /// Home page Controller
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Simple Homepage
        /// </summary>
        /// <returns>homepage view - Home/Index</returns>
        public ViewResult Index()
        {
            return View();
        }
    }
}