﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoTunes.Controllers
{
    /// <summary>
    /// Error Handling Controller
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// Simple Error Handler
        /// </summary>
        /// <returns>error page view - Error/Index</returns>
        public ViewResult Index()
        {
            // pass any message into a viewmodel
            object msg = TempData["Message"] as object;
            return View(msg);
        }
    }
}