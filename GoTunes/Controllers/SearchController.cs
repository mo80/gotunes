﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Net.Http;
using GoTunes.Models;
using System.Threading;
using System.Configuration;

namespace GoTunes.Controllers
{
    /// <summary>
    /// Search Controller
    /// </summary>
    public class SearchController : Controller
    {
        // instance of the API Helper
        private APIService service = new APIService();

        /// <summary>
        /// Asynchronous API Search
        /// </summary>
        /// <param name="SearchText">Search term entered by the user and passed by the view</param>
        /// <returns>Search View - Search/Index with async iTunesResultSet</returns>
        // HttpPost as this ActionResult accepts posted search term from the view
        [HttpPost]
        // outputcaches the view based on the search term - good performance gain to avoid unnecessary repeated API calls or roundtrips
        // configurable cache duration through CacheProfile setting - config file
        [OutputCache(CacheProfile = "CacheProfileHour", VaryByParam = "SearchText")]
        public async Task<ActionResult> Index(string SearchText)
        {
            try
            {
                // await and get results async thru the API Helper object
                return View(await service.GetResultsAsync(SearchText));
            }
            catch (Exception ex)
            {
                // catch the timeout exception
                if (ex is TimeoutException)
                {
                    // Pass timeout Error description to show
                    TempData["Message"] = "The operation has timed out";
                    // redirect to error action
                    return RedirectToAction("Error");
                    //return View("Error");
                }
                else
                {
                    // some other exception with API call
                    TempData["Message"] = "Try again later";
                    // redirect to error action
                    return RedirectToAction("Error");
                    //return View("Error");
                }
            }

        }
    }
}