﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using System.Configuration;


namespace GoTunes.Models
{
    /// <summary>
    /// iTunes API Helper Class
    /// </summary>
    public class APIService
    {
        // Base Url of iTunes API
        private readonly string Url = ConfigurationManager.AppSettings["APIBaseUrl"].ToString();
        private HttpClient client;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query">Search term</param>
        /// <returns>Asynchronous Task of iTunesResultSet</returns>
        public async Task<iTunesResultSet> GetResultsAsync(string term)
        {
            iTunesResultSet resultList = null;
            // Instantiate an HttpClient object to make the API call
            // using here ensures disposing the idisposable httpclient object correctly after we are done
            using (client = new HttpClient())
            {
                // set the base url
                client.BaseAddress = new Uri(Url);
                // clear header
                client.DefaultRequestHeaders.Accept.Clear();
                // set media format to json
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = null;
                // set the api call to an async task
                Task<HttpResponseMessage> task = client.GetAsync("/search/?term=" + term);
                // Call the Api and wait for the result
                // the blocking part using await
                // No need to wait for long time thus the implementation of timeout
                // timeout is a configurable variable APITimeout - config file
                // Due to the lack of better timeout implementation .NET for asyn/await
                // this is one of the potential and suggested by MS timeouts implementations
                // using Task.Delay in conjunction with Task.WhenAny
                // compares the original api task with a dummy timed task and returns the first task to complete
                if (task == await Task.WhenAny(task, Task.Delay(Convert.ToInt32(ConfigurationManager.AppSettings["APITimeout"].ToString()))))
                    // API task completed before timeout
                    // get the response message
                    response = ((HttpResponseMessage) await task);
                else
                    // timeout occured before getting API results
                    // throw timeout exception that is caught in the calling search controller
                    throw new TimeoutException();
                // check if the response message returned with success code
                if (response.IsSuccessStatusCode)
                {
                    // get results async - in case of large responses this can be helpful
                    // Deserialize the JSON object with JavascriptSerializer into iTunesResultSet
                    resultList = new JavaScriptSerializer().Deserialize<iTunesResultSet>(
                        await response.Content.ReadAsStringAsync());
                }
            }
            // return result
            return resultList;
        }

        
    }
}