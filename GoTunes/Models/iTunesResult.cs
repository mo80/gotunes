﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoTunes.Models
{
    /// <summary>
    /// The iTunes Result Class
    /// </summary>
    public class iTunesResult
    {
        /// <summary>
        /// The Name of The Artist if available
        /// </summary>
        public string ArtistName { get; set; }
        /// <summary>
        /// Collection or Album name if available
        /// </summary>
        public string CollectionName { get; set; }
        /// <summary>
        /// Title of the entry (Track or Song or Movie...)
        /// </summary>
        public string TrackName { get; set; }
        /// <summary>
        /// View Url of the entry
        /// </summary>
        public string TrackViewUrl { get; set; }
        /// <summary>
        /// Thumbnail or Artwork 100px x 100px
        /// </summary>
        public string ArtworkUrl100 { get; set; }
    }
}