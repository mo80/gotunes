﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoTunes.Models
{
    /// <summary>
    /// The collection set that contains all the returned results
    /// </summary>
    public class iTunesResultSet
    {
        /// <summary>
        /// List of returned results of type iTunesResult 
        /// </summary>
        public List<iTunesResult> results { get; set; }
    }
}